module.exports = {
  presets: ['module:metro-react-native-babel-preset', '@babel/preset-react'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '^react-native$': 'react-native-web',
        },
        extensions: ['.js', '.jsx', '.tsx', '.json', '.ios.js', '.android.js'],
      },
    ],

    'react-native-reanimated/plugin',
  ],
};
//
