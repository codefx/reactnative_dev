import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const ViewPager = ({children}) => {
  return (
    <SafeAreaView style={styles.main}>
      <View style={styles.container}>
        <View style={styles.leftpart}>
          <Text style={styles.textStyle}>left content</Text>
          <Text style={styles.textStyle}>left content</Text>
          <Text style={styles.textStyle}>left content</Text>
          <Text style={styles.textStyle}>left content</Text>
        </View>
        <View style={styles.rightPart}>
          {/* PagerHAeader */}
          <View style={styles.rightHeader}>
            <View style={styles.rightHeaderInnerLeft}>
              <Text style={styles.textStyle}>headerLeft</Text>
            </View>
            <View style={styles.rightHeaderInnerRight}>
              <Text style={styles.textStyle}>headerRight</Text>
              <Text style={styles.textStyle}>headerRight</Text>
              <Text style={styles.textStyle}>headerRight</Text>
            </View>
          </View>

          {/* content */}
          <View style={styles.rightBody}>
            {/* pages */}
            <Text style={styles.textStyle}>pages content</Text>
            <Text style={styles.textStyle}>pages content</Text>
            <Text style={styles.textStyle}>pages content</Text>
            <Text style={styles.textStyle}>pages content</Text>
            <Text style={styles.textStyle}>pages content</Text>
          </View>

          <View style={styles.rightFooter}>
            <View style={styles.rightFooterRight}>
              <Text style={styles.textStyle}>Footer Buttons</Text>
              <Text style={styles.textStyle}>Footer Buttons</Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ViewPager;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    margin: 10,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  partContainer: {
    flex: 1,
  },
  textStyle: {},
  leftpart: {
    flex: 1,
  },
  rightPart: {
    flex: 5,
    gap: 5,
  },
  rightHeader: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#efd3ad',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  rightHeaderInnerLeft: {
    alignContent: 'flex-start',
    marginLeft: 10,
    backgroundColor: '#d8afe6',
  },
  rightHeaderInnerRight: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: '#9ca5ed',
    marginRight: 10,
  },
  rightBody: {
    flex: 5,
    backgroundColor: '#93d7c3',
  },
  rightFooter: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#c0bcf0',
    justifyContent: 'flex-end',
  },
  rightFooterRight: {
    flexDirection: 'row',
    backgroundColor: '#c0eaad',
    justifyContent: 'flex-end',
    height: 50,
    gap: 10,
    marginRight: 20,
    padding: 10,
  },
});
