import {Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Image} from 'react-native';
const logo = require('../assets/images/dorukicon.jpg');

const MyDrawerIcon = ({onPress}) => {
  return (
    <Pressable onPress={() => onPress()}>
      <Image source={logo} style={{width: 50, height: 50}} resizeMode="cover" />
    </Pressable>
  );
};

export default MyDrawerIcon;

const styles = StyleSheet.create({});
