import {AppRegistry} from 'react-native';
import App from './App';
const appName = 'navs';
AppRegistry.registerComponent(appName, () => App);
AppRegistry.runApplication(appName, {
  rootTag: document.getElementById('root'),
});

// in package.json to scripts section add this:

// "web":"react-scripts start",

// npm run web
// in console  type y

// $ react-scripts start ? We're unable to detect target browsers.
// Would you like to add the defaults to your package.json? » (Y/n)
// it some added into package.json file
