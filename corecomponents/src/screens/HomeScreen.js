import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  StyleSheet,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, width: '100%', alignItems: 'center'}}>
      <View style={styles.container}>
        <Text style={styles.text}>You are on Home Screen</Text>
      </View>
      <ScrollView style={{flex: 3, width: '100%', padding: 16}}>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#bdf3bd'}}
          onPress={() => navigation.navigate('SectionLists')}>
          <Text style={{color: '#406cf5', fontSize: 20}}>
            {' '}
            Go to Section list screen
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#935966'}}
          onPress={() => navigation.navigate('DrawerLayouts')}>
          <Text style={{color: '#9fa9af', fontSize: 20}}>
            Open DrawerLayout Screen
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  topacity: {
    margin: 10,
    flex: 1,
    width: '85%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#bdf3bd',
    borderRadius: 28,
  },
  text: {
    fontSize: 21,
    textAlign: 'center',
    marginBottom: 16,
  },
});

export default HomeScreen;
