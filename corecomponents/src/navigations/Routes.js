import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import SectionLists from '../screens/SectionLists';
import DrawerLayouts from '../screens/DrawerLayouts';

const Stack = createStackNavigator();
const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="SectionLists"
          component={SectionLists}
          options={{
            title: 'SectionLists Page',
            headerStyle: {
              backgroundColor: '#7bcc62',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="DrawerLayouts"
          component={DrawerLayouts}
          options={{
            title: 'DrawerLayouts Page',
            headerStyle: {
              backgroundColor: '#67e8cc',
            },

            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
