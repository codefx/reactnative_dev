import React from 'react';

import ThemeCtxProvider from './contexts/ThemeContext';
import Main from './Main';
import Napp from './Napp';

import 'react-native-gesture-handler';

const App = () => {
  return (
    <ThemeCtxProvider>
      {/* <Main /> */}
      <Napp />
    </ThemeCtxProvider>
  );
};
export default App;
