import {StyleSheet, Text, View} from 'react-native';
import React, {createContext, useLayoutEffect, useState} from 'react';

import {lightTheme, darkTheme, themeSelector} from '../theme/theme';

export const ThemeContext = createContext({});
const ThemeCtxProvider = ({children}) => {
  const light = 'light';
  const dark = 'dark';

  const [isSelected, setIsSelected] = useState(false);
  const [appThemeName, setAppThemeName] = useState(light);
  const [currTheme, setCurrTheme] = useState(lightTheme);

  const themeSelector = currTheme => {
    const selectedTheme =
      currTheme?.name === lightTheme.name ? darkTheme : lightTheme;
    return selectedTheme;
  };

  const toggleThemeName = () => {
    if (appThemeName == light) {
      setAppThemeName(dark);
      setCurrTheme(() => themeSelector(currTheme));
    } else {
      setAppThemeName(light);
      setCurrTheme(() => themeSelector(currTheme));
    }
  };

  // useEffect(() => {
  //   toggleThemeName();
  // }, [appThemeName, currTheme, toggleThemeName]);

  const values = {
    appThemeName,
    setAppThemeName,
    lightTheme,
    darkTheme,
    currTheme,
    setCurrTheme,

    toggleThemeName,
  };
  return (
    <ThemeContext.Provider value={values}>{children}</ThemeContext.Provider>
  );
};

export default ThemeCtxProvider;
