import {Pressable, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {ThemeContext} from './contexts/ThemeContext';

const Main = () => {
  const {currTheme, toggleThemeName} = useContext(ThemeContext);
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: currTheme?.backgroundColor1,
      }}>
      <View
        style={{
          ...styles.main,
          //backgroundColor: currTheme?.backgroundColor5,
        }}>
        <Pressable
          onPress={() => {
            toggleThemeName();
            // alert('hii from appp');
          }}
          style={{
            ...styles.pressable,
            backgroundColor: currTheme?.backgroundColor3,
          }}>
          <Text style={{color: currTheme?.textColor}}>Hi From uuu</Text>
        </Pressable>
      </View>
    </SafeAreaView>
  );
};

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  main: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  pressable: {
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#0f0',
    padding: 5,
    width: '100%',
    height: 50,
    borderRadius: 25,
    marginTop: 10,
  },
  text: {},
});
