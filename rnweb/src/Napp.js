import React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import {createDrawerNavigator} from '@react-navigation/drawer';

import SettingScreenStack from './navigations/SettingScreenStack';

import MyDrawerIcon from './components/MyDrawerIcon';
import CustomDrawer from './components/CustomDrawer';
import dataList from './data/dummy';
import {deviceWidth} from './utils/deviceDimensions';

const Drawer = createDrawerNavigator();

const Napp = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        screenOptions={({navigation}) => ({
          headerLeft: () => <MyDrawerIcon onPress={navigation.toggleDrawer} />,
          drawerType: deviceWidth >= 768 ? 'permanent' : 'front',
          overlayColor: 'transparent',
          headerStyle: {
            backgroundColor: '#5096f2', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          drawerStyle: {
            backgroundColor: '#3a5267e6',
            width: deviceWidth * 0.135,
          },
          itemStyle: {marginVertical: 5},
          labelStyle: {fontSize: 16},
          drawerActiveTintColor: '#fff',
          drawerInactiveTintColor: '#999',
        })}
        drawerContent={props => <CustomDrawer {...props} />}>
        {dataList.map(item => {
          return (
            <Drawer.Screen
              key={item.name}
              name={item.name}
              options={{
                drawerLabel: `${item.label}`,
                title: `${item.title}`,
              }}
              component={item.component}
            />
          );
        })}

        {/* <Drawer.Screen
          name="HomeScreenStack"
          options={{
            drawerLabel: 'Home Screen Option',
            title: 'Home Screen',
          }}
          component={HomeScreenStack}
        /> */}
        <Drawer.Screen
          name="SettingScreenStack"
          options={{
            drawerLabel: 'Setting Screen Option',
            title: 'Setting Screen',
          }}
          component={SettingScreenStack}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default Napp;
