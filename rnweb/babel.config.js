module.exports = {
  presets: [
    'module:metro-react-native-babel-preset',
    '@babel/env',
    '@babel/preset-react',
  ],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.tsx', '.ts', '.js', '.jsx', '.json'],
      },
    ],
    '@babel/plugin-syntax-jsx',
    '@babel/plugin-proposal-export-namespace-from',
    'react-native-reanimated/plugin',
  ],
};
