/* eslint-disable prettier/prettier */
import React, {createContext, useState} from 'react';
import {LayoutAnimation} from 'react-native';
import dataList from '../data/dorukNavData';

export const NavContext = createContext({});

const NavCtxProvider = ({children}) => {
  const [listDataSource, setListDataSource] = useState(dataList);

  const [layoutHeight, setLayoutHeight] = useState(0);

  const updateLayout = index => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...listDataSource];
    // If single select is enabled
    array.map((value, placeindex) =>
      placeindex === index
        ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
        : (array[placeindex]['isExpanded'] = false),
    );
    setListDataSource(array);
  };

  const values = {
    listDataSource,
    setListDataSource,
    updateLayout,
    layoutHeight,
    setLayoutHeight,
  };
  return <NavContext.Provider value={values}>{children}</NavContext.Provider>;
};

export default NavCtxProvider;
