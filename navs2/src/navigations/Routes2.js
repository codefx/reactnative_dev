import React from 'react';

import {NavigationContainer} from '@react-navigation/native';

import CustomDrawerNavigator from '../components/CustomDrawerNavigator';

import NavCtxProvider from '../contexts/NavContext';

const Routes2 = () => {
  return (
    <NavigationContainer>
      <NavCtxProvider>
        <CustomDrawerNavigator />
      </NavCtxProvider>
    </NavigationContainer>
  );
};

export default Routes2;
