import HomeScreenStack from '../navigations/HomeScreenStack';
import Qw1Screen from '../screens/Qw1Screen';
import Qw2Screen from '../screens/Qw2Screen';
import Qw3Screen from '../screens/Qw3Screen';
const dataList = [
  {
    name: 'HomeScreenStack',
    label: 'Home Screen Option',
    title: 'Home Screen',
    component: HomeScreenStack,
  },
  {
    name: 'qw1 Screen',
    label: 'Rota Screen Option',
    title: 'qw1 Screen',
    component: Qw1Screen,
    submenu: {
      sub1: 'sub1',
      sub2: 'sub2',
    },
  },
  {
    name: 'qw2 Screen',
    label: 'Siparişler Option',
    title: 'qw2 Screen',
    component: Qw2Screen,
    submenu: {
      sub1: 'sub1',
      sub2: 'sub2',
    },
  },
  {
    name: 'qw3 Screen',
    label: 'Users Screen Option',
    title: 'qw3 Screen',
    component: Qw3Screen,
    submenu: {
      sub1: 'sub1',
      sub2: 'sub2',
    },
  },
];
export default dataList;
