import Qw1Screen from '../screens/Qw1Screen';
import Qw2Screen from '../screens/Qw2Screen';
import Qw3Screen from '../screens/Qw3Screen';
const dorukNavData = [
  {
    isExpanded: false,
    category_name: 'Home',
    subcategory: [
      {
        id: 0,
        name: 'HomeScr',
        label: 'Arama / Listeleme Option',
        title: 'Home /Arama - Listeleme Screen',
        component: Qw1Screen,
      },
    ],
  },
  {
    isExpanded: false,
    category_name: 'Rota',
    subcategory: [
      {
        id: 1,
        name: 'AramaListeleme',
        label: 'Arama / Listeleme Option',
        title: 'Rota/ Arama-Listeleme Screen',
        component: Qw1Screen,
      },
      {
        id: 2,
        name: 'RotaAtama',
        label: 'Rota Atama Option',
        title: 'Rota Atama Screen',
        component: Qw1Screen,
      },
      {
        id: 3,
        name: 'AracSevkiyat',
        label: 'Araç Sevkiyat Option',
        title: 'Rota/ Araç Sevkiyat Screen',
        component: Qw1Screen,
      },
      {
        id: 4,
        name: 'SevkiyatIptal',
        label: 'Sevkiyat İptal Option',
        title: 'Rota /Sevkiyat İptal Screen',
        component: Qw1Screen,
      },
    ],
  },
  {
    isExpanded: false,
    category_name: 'Siparişler',
    subcategory: [
      {
        id: 5,
        name: 'IrsaliyesiKesilmemisler',
        label: 'İrsaliyesi Kesilmemişler Option',
        title: 'İrsaliyesi Kesilmemişler Screen',
        component: Qw2Screen,
      },
    ],
  },
  {
    isExpanded: false,
    category_name: 'Users',
    subcategory: [
      {
        id: 6,
        name: 'AraListele',
        label: 'Ara Listele Option',
        title: 'Users /Ara Listele Screen',
        component: Qw3Screen,
      },
      {
        id: 7,
        name: 'Ekle',
        label: 'Ekle Option',
        title: 'Users /Ekle Screen',
        component: Qw3Screen,
      },
    ],
  },
];
export default dorukNavData;
