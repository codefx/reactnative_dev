import {Button, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const LandingPage = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1, padding: 16}}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              marginBottom: 16,
            }}>
            Dynamically Set Drawer/Sidebar Options
            {'\n'}
            in React Navigation Drawer
            {'\n\n'}
            Landing Page
          </Text>
          <Button
            onPress={() =>
              navigation.navigate('DrawerStack', {userType: 'user'})
            }
            title="Go to Home as Registerd User"
          />
          <Text
            style={{
              textAlign: 'center',
              marginVertical: 16,
            }}>
            ------------- OR -------------
          </Text>
          <Button
            onPress={() =>
              navigation.navigate('DrawerStack', {userType: 'guest'})
            }
            title="Go to Home as Guest"
          />
        </View>
        <Text
          style={{
            fontSize: 18,
            textAlign: 'center',
            color: 'grey',
          }}>
          Dynamically Set Drawer/Sidebar Options
        </Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: 'center',
            color: 'grey',
          }}>
          www.aboutreact.com
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default LandingPage;

const styles = StyleSheet.create({});
