import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import FirstScreenStack from './FirstScreenStack';
import SecondScreenStack from './SecondScreenStack';
import ThirdScreenStack from './ThirdScreenStack';

const Drawer = createDrawerNavigator();
const DrawerStack = ({route}) => {
  return (
    <Drawer.Navigator
      drawerContent={props => {
        return (
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            {route.params.userType === 'user' ? (
              <DrawerItem
                label={({color}) => (
                  <Text style={{color}}>Change Access to Guest</Text>
                )}
                onPress={() =>
                  props.navigation.navigate('DrawerStack', {userType: 'guest'})
                }
              />
            ) : null}
          </DrawerContentScrollView>
        );
      }}>
      <Drawer.Screen
        name="FirstScreenStack"
        options={{drawerLabel: 'First page Option'}}
        component={FirstScreenStack}
        initialParams={{userType: route.params.userType}}
      />
      {route.params.userType === 'user' ? (
        <>
          <Drawer.Screen
            name="SecondScreenStack"
            options={{drawerLabel: 'Second page Option'}}
            component={SecondScreenStack}
          />
          <Drawer.Screen
            name="ThirdScreenStack"
            options={{drawerLabel: 'Third page Option'}}
            component={ThirdScreenStack}
          />
        </>
      ) : null}
    </Drawer.Navigator>
  );
};

export default DrawerStack;

const styles = StyleSheet.create({});
