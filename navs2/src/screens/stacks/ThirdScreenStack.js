import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ThirdPage from '../pages/ThirdPage';

const Stack = createNativeStackNavigator();
const ThirdScreenStack = ({route}) => {
  return (
    <Stack.Navigator
      initialRouteName="ThirdPage"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="ThirdPage"
        component={ThirdPage}
        initialParams={route.params}
      />
    </Stack.Navigator>
  );
};

export default ThirdScreenStack;

const styles = StyleSheet.create({});
