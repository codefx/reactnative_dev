import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SecondPage from '../pages/SecondPage';

const Stack = createNativeStackNavigator();
const SecondScreenStack = ({route}) => {
  return (
    <Stack.Navigator
      initialRouteName="SecondPage"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="SecondPage"
        component={SecondPage}
        initialParams={route.params}
      />
    </Stack.Navigator>
  );
};

export default SecondScreenStack;

const styles = StyleSheet.create({});
