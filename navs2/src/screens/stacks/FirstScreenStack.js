import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import FirstPage from '../pages/FirstPage';

const Stack = createNativeStackNavigator();
const FirstScreenStack = ({route}) => {
  return (
    <Stack.Navigator
      initialRouteName="FirstPage"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="FirstPage"
        component={FirstPage}
        initialParams={route.params}
      />
    </Stack.Navigator>
  );
};

export default FirstScreenStack;

const styles = StyleSheet.create({});
