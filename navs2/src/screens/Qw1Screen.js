import React from 'react';

import {Button, SafeAreaView, Text, View} from 'react-native';

const Qw1Screen = ({route, navigation}) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          width: '90%',
        }}>
        <Button
          onPress={() => navigation.navigate('HomeScreen')}
          title="Goto home"
        />
        <Text
          style={{
            color: '#52b752',
            fontSize: 24,
            fontWeight: '400',
            marginBottom: 20,
            marginTop: 20,
          }}>
          ' hii from qw'
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default Qw1Screen;
