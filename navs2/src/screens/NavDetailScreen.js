import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import dims from '../utils/deviceDimensions';

const NavDetailScreen = ({route, navigation}) => {
  const item = route.params;
  return (
    <View
      style={{
        width: '100%',
        alignItems: 'center',
        paddingVertical: 28,
        paddingHorizontal: 22,
      }}>
      <Text style={{fontSize: 18}}>hello nav screen</Text>
      {/* <Image
        style={{
          resizeMode: 'cover',
          width: '100%',
          height: 90,
          margin: 15,
        }}
        source={item.image}
      /> */}
      {/* <Text style={{fontSize: 28, marginBottom: 15}}>{item.title}</Text>
      <Text style={{fontSize: 18}}>{item.detail}</Text> */}
    </View>
  );
};
export default NavDetailScreen;
