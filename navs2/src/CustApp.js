import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

import FirstPage from './screens/pages2/FirstPage';
import SecondPage from './screens/pages2/SecondPage';
import ThirdPage from './screens/pages2/ThirdPage';

// Import Custom Sidebar
import CustomSidebarMenu from './components/sidebar/CustomSidebarMenu';
import CustomSidebarMenu2 from './components/sidebar/CustomSidebarMenu2';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

function FirstScreenStack() {
  return (
    <Stack.Navigator
      initialRouteName="FirstPage"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="FirstPage" component={FirstPage} />
    </Stack.Navigator>
  );
}

function SecondScreenStack() {
  return (
    <Stack.Navigator
      initialRouteName="SecondPage"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="SecondPage"
        component={SecondPage}
        options={{
          title: 'Second Page', //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
}

function ThirdScreenStack() {
  return (
    <Stack.Navigator
      initialRouteName="ThirdPage"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="ThirdPage"
        component={ThirdPage}
        options={{
          title: 'Third Page', //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#f4511e', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
        }}
        // For setting Custom Sidebar Menu
        drawerContent={props => <CustomSidebarMenu {...props} />}>
        <Drawer.Screen
          name="FirstScreenStack"
          options={{
            drawerLabel: 'First page Option',
            title: 'First Stack',
            // Section/Group Name
            groupName: 'Section 1',
            activeTintColor: '#e91e63',
          }}
          component={FirstScreenStack}
        />
        <Drawer.Screen
          name="SecondScreenStack"
          options={{
            drawerLabel: 'Second page Option',
            title: 'Second Stack',
            // Section/Group Name
            groupName: 'Section 2',
            activeTintColor: '#e91e63',
          }}
          component={SecondScreenStack}
        />
        <Drawer.Screen
          name="ThirdScreenStack"
          options={{
            drawerLabel: 'Third page Option',
            title: 'Third Stack',
            // Section/Group Name
            groupName: 'Section 2',
            activeTintColor: '#e91e63',
          }}
          component={ThirdScreenStack}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
