import {Platform, ScrollView, UIManager, View} from 'react-native';
import React, {useContext} from 'react';

import MyExpandableComponent from './MyExpandableComponent';
import {NavContext} from '../contexts/NavContext';

const ExpandableList = ({props}) => {
  const {listDataSource, updateLayout} = useContext(NavContext);

  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  return (
    <ScrollView>
      {listDataSource.map((item, key) => (
        <MyExpandableComponent
          props={props}
          key={item.category_name}
          updateLayout={() => {
            updateLayout(key);
          }}
          dataItem={item}
        />
      ))}
    </ScrollView>
  );
};

export default ExpandableList;
