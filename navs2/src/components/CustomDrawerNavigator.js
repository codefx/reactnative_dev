import {StyleSheet} from 'react-native';
import React, {useContext} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomSidebarMenu2 from './sidebar/CustomSidebarMenu2';
import {NavContext} from '../contexts/NavContext';
import {deviceWidth} from '../utils/deviceDimensions';

const Drawer = createDrawerNavigator();
const CustomDrawerNavigator = () => {
  const {listDataSource} = useContext(NavContext);

  return (
    <Drawer.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#1e3175', //Set Header color
        },
        headerTintColor: '#fff', //Set Header text color
        drawerActiveTintColor: '#ffffff',
        drawerStyle: {
          width: deviceWidth * 0.135,
        },
      }}
      // For setting Custom Sidebar Menu
      drawerContent={props => <CustomSidebarMenu2 props={props} />}>
      {listDataSource.map((item, key) => {
        return item.subcategory.map(subItem => {
          return (
            <Drawer.Screen
              key={subItem.id}
              name={subItem.name}
              options={{
                drawerLabel: `${subItem.label}`,
                title: `${subItem.title}`,
              }}
              component={subItem.component}
            />
          );
        });
      })}
    </Drawer.Navigator>
  );
};

export default CustomDrawerNavigator;

const styles = StyleSheet.create({});
