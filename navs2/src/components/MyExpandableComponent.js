import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import {deviceWidth} from '../utils/deviceDimensions';
import {DrawerItem} from '@react-navigation/drawer';
//import {NavContext} from '../contexts/NavContext';

const MyExpandableComponent = ({props, dataItem, updateLayout}) => {
  const [layoutHeight, setLayoutHeight] = useState(0);
  const {state, descriptors, navigation} = props;
  useEffect(() => {
    if (dataItem.isExpanded) {
      setLayoutHeight(null);
    } else {
      setLayoutHeight(0);
    }
  }, [dataItem.isExpanded, setLayoutHeight]);

  return (
    <View>
      {/*Header of the Expandable List Item*/}
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={updateLayout}
        style={styles.header}>
        {/* <MIcon name={dataItem.iconName} size={16} color="#67d956" /> */}
        <Text style={styles.headerText}>{dataItem.category_name}</Text>
      </TouchableOpacity>
      <View style={{height: layoutHeight, overflow: 'hidden'}}>
        {/*Content under the header of the Expandable List Item*/}
        {dataItem.subcategory.map((item, key) => {
          return (
            <>
              <DrawerItem
                key={key}
                style={styles.content}
                label={({color}) => (
                  <Text style={{color: color}}>{item.label}</Text>
                )}
                focused={
                  state.routes.findIndex(e => e.name === item.name) ===
                  state.index
                }
                onPress={() => navigation.navigate(item.name)}
              />
              <View style={styles.separator} />
            </>
          );
        })}
      </View>
    </View>
  );
};

export default MyExpandableComponent;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
    marginLeft: 5,
  },
  headerText: {
    fontSize: 16,
    fontWeight: '500',
  },
  separator: {
    height: 0.5,
    backgroundColor: '#808080',
    width: '95%',
    marginLeft: 16,
    marginRight: 16,
  },
  text: {
    fontSize: 16,
    color: '#606070',
    padding: 10,
  },
  content: {
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#fff',
  },
});
