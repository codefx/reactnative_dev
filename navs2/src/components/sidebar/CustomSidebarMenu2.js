import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';

import {DrawerContentScrollView} from '@react-navigation/drawer';
import CustomDrawerTop from '../CustomDrawerTop';
import ExpandableList from '../ExpandableList';
import CustomDrawerBottom from '../CustomDrawerBottom';

const CustomSidebarMenu2 = ({props}) => {
  return (
    <SafeAreaView style={styles.main}>
      <CustomDrawerTop props={props} />
      <DrawerContentScrollView {...props}>
        <ExpandableList props={props} />
      </DrawerContentScrollView>
      <CustomDrawerBottom props={props} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  main: {flex: 1},
});

export default CustomSidebarMenu2;
