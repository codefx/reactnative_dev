import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
const logo = require('../assets/images/dorukicon.jpg');

const CustomDrawerBottom = ({props}) => {
  return (
    <Pressable onPress={props.navigation.toggleDrawer}>
      <View style={styles.logoView}>
        <View>
          <Text>Çıkış</Text>
        </View>
        <Image source={logo} style={styles.logo} resizeMode="cover" />
      </View>
    </Pressable>
  );
};

export default CustomDrawerBottom;

const styles = StyleSheet.create({
  logoView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#f6f6f6',
    marginBottom: 20,
  },
  logo: {width: 50, height: 50},
});
