import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import CustomDrawerTop from './CustomDrawerTop';

const CustomDrawer = props => {
  return (
    <View style={styles.main}>
      <DrawerContentScrollView {...props}>
        <CustomDrawerTop props={props} />
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <Pressable style={styles.pressable}>
        <Text>Log Out</Text>
      </Pressable>
    </View>
  );
};

export default CustomDrawer;

const styles = StyleSheet.create({
  main: {flex: 1},
  pressable: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 50,
    backgroundColor: '#f6f6f6',
    padding: 20,
  },
});
