import React from 'react';
import {ToastAndroid} from 'react-native';

export const showToast = () => {
  ToastAndroid.show('A pikachu appeared nearby !', ToastAndroid.SHORT);
};

export const showToastWithGravity = () => {
  //showWithGravity(message, duration, gravity)

  //duration
  //ToastAndroid.SHORT or
  //ToastAndroid.LONG

  //gravity
  //ToastAndroid.TOP,
  // ToastAndroid.BOTTOM or
  //ToastAndroid.CENTER
  ToastAndroid.showWithGravity(
    'All Your Base Are Belong To Us',
    ToastAndroid.SHORT,
    ToastAndroid.CENTER,
  );
};

export const showToastWithGravityAndOffset = () => {
  //showWithGravityAndOffset(message, duration, gravity, xOffset, yOffset
  ToastAndroid.showWithGravityAndOffset(
    'A wild toaaassst appeared!',
    ToastAndroid.LONG,
    ToastAndroid.TOP,
    25,
    50,
  );
};

export const imperativeToast = ({visible, message}) => {
  //to runnning comp=>
  // const [visibleToast, setVisibleToast] = useState(false);
  // useEffect(() => setVisibleToast(false), [visibleToast]);
  // const handleButtonPress = () => {setVisibleToast(true);};
  // <Toast visible={visibleToast} message="Example" />
  // <Button title="Toggle Toast" onPress={() => handleButtonPress()} />;
  if (visible) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      36,
      62,
    );
    return null;
  }
  return null;
};
