import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Button,
  BackHandler,
  Alert,
  TouchableOpacity,
} from 'react-native';
import requestCameraPermission from '../components/requestCameraPermission';

const PermissionsScreen = ({navigation}) => {
  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to go back?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  return (
    <SafeAreaView style={{flex: 1, width: '100%', alignItems: 'center'}}>
      <View style={{width: '100%', height: '100%'}}>
        <View style={styles.container}>
          <Text style={styles.item}>Try permissions</Text>
          <Button
            title="request permissions"
            onPress={() => requestCameraPermission()}
          />
        </View>
        <Text style={styles.text}>Click Back button!</Text>
        <TouchableOpacity
          style={{...styles.topacity}}
          onPress={() => navigation.navigate('Home')}>
          <Text style={{color: '#be1111', fontSize: 20}}>
            {' '}
            Go to Home Screen no press back otherwise you will exit
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#87eb7e',
    padding: 8,
  },
  item: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: '#7acbdf',
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  topacity: {
    margin: 10,
    flex: 1,
    width: '85%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#bdf3bd',
    borderRadius: 28,
  },
});

export default PermissionsScreen;
