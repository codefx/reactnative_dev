import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  StyleSheet,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, width: '100%', alignItems: 'center'}}>
      <View style={styles.container}>
        <Text style={styles.text}>You are on Home Screen</Text>
      </View>
      <ScrollView style={{flex: 3, width: '100%', padding: 16}}>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#bdf3bd'}}
          onPress={() => navigation.navigate('Permission')}>
          <Text style={{color: '#406cf5', fontSize: 20}}>
            {' '}
            Go to Permissions
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#935966'}}
          onPress={() => navigation.navigate('Toast')}>
          <Text style={{color: '#9fa9af', fontSize: 20}}>
            Open Toasts Screen
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{...styles.topacity}}
          onPress={() => navigation.navigate('Vibrations')}>
          <Text style={{color: '#be1111', fontSize: 20}}> Go to Vibrating</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.topacity}}
          onPress={() => navigation.navigate('Share')}>
          <Text style={{color: '#5b80ee', fontSize: 20}}> Go to Sharing</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#a1e1d9'}}
          onPress={() => navigation.navigate('PixRat')}>
          <Text style={{color: '#5933a6', fontSize: 20}}>
            {' '}
            Go to Pixel Ratio
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#38f638'}}
          onPress={() => navigation.navigate('Transforms')}>
          <Text style={{color: '#fdfdfd', fontSize: 20}}>
            {' '}
            Go to Transforms
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  topacity: {
    margin: 10,
    flex: 1,
    width: '85%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#bdf3bd',
    borderRadius: 28,
  },
  text: {
    fontSize: 21,
    textAlign: 'center',
    marginBottom: 16,
  },
});

export default HomeScreen;
