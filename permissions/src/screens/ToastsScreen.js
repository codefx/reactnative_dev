import React, {useEffect, useState} from 'react';
import {View, Text, SafeAreaView, Button} from 'react-native';
import {
  showToast,
  showToastWithGravity,
  showToastWithGravityAndOffset,
} from '../components/toastAndroids';
import {imperativeToast as Toast} from '../components/toastAndroids';

const ToastsScreen = ({navigation}) => {
  const [visibleToast, setVisibleToast] = useState(false);
  useEffect(() => setVisibleToast(false), [visibleToast]);

  const handleButtonPress = () => {
    setVisibleToast(true);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1, padding: 16}}>
        <Button title="Toggle Toast" onPress={() => showToast()} />
        <Button
          title="Toggle Toast With Gravity"
          onPress={() => showToastWithGravity()}
        />
        <Button
          title="Toggle Toast With Gravity & Offset"
          onPress={() => showToastWithGravityAndOffset()}
        />
        <Toast visible={visibleToast} message="imperative toast" />
        <Button
          title="Toggle imperative Toast"
          onPress={() => handleButtonPress()}
        />
      </View>
    </SafeAreaView>
  );
};
export default ToastsScreen;
