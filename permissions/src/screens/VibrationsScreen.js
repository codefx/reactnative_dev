import * as React from 'react';
import {View, SafeAreaView} from 'react-native';
import Vibrating from '../components/vibrations';

const VibrationsScreen = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1, padding: 16}}>
        <Vibrating />
      </View>
    </SafeAreaView>
  );
};
export default VibrationsScreen;
