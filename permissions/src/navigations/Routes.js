import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import PermissionsScreen from '../screens/PermissionsScreen';
import ToastsScreen from '../screens/ToastsScreen';
import VibrationsScreen from '../screens/VibrationsScreen';
import ShareScreen from '../screens/ShareScreen';
import PixelRatioScreen from '../screens/PixelRatioScreen';
import TransformsScreen from '../screens/TransformsScreen';

const Stack = createStackNavigator();
const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Permission"
          component={PermissionsScreen}
          options={{
            title: 'Setting Page',
            headerStyle: {
              backgroundColor: '#7bcc62',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Toast"
          component={ToastsScreen}
          options={{
            title: 'Toasts screen',
            headerStyle: {
              backgroundColor: '#67e8cc',
            },

            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Vibrations"
          component={VibrationsScreen}
          options={{
            title: 'Vibrations samples',
            headerStyle: {
              backgroundColor: '#56b4d7',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Share"
          component={ShareScreen}
          options={{
            title: 'Vibrations samples',
            headerStyle: {
              backgroundColor: '#56b4d7',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="PixRat"
          component={PixelRatioScreen}
          options={{
            title: 'PixelRatio samples',
            headerStyle: {
              backgroundColor: '#56b4d7',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Transforms"
          component={TransformsScreen}
          options={{
            title: 'Transforms samples',
            headerStyle: {
              backgroundColor: '#56b4d7',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
